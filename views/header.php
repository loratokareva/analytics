<header class="header d-flex align-items-center">
    <div class="container">
        <div class="row align-items-center g-0">
            <div class="col">
                <a href="/" class="title-site">
                    Аналитика аренды
                </a>
            </div>
            <div class="col-4 col-lg-3">
               <section class="search">
                   <form action="">
                       <div class="input-group">
                           <input type="text" class="form-control" placeholder="Поиск объекта">
                           <button class="btn btn-search" type="button"></button>
                       </div>
                   </form>
               </section>
            </div>
        </div>
    </div>
</header>