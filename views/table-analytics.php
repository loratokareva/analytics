<!--    таблица с аналитикой    -->
<table class="table  table-bordered table-striped table_analytics">
    <tr>
        <td>
            Общая площадь аренды
        </td>
        <td class="text-center">
            100 000 м<sup>2</sup>
        </td>
    </tr>
    <tr>
        <td>
            Текущая площадь аренды
        </td>
        <td class="text-center">
            80 000 м<sup>2</sup>
        </td>
    </tr>
    <tr>
        <td>
            Свободная площадь аренды
        </td>
        <td class="text-center">
            20 000 м<sup>2</sup>
        </td>
    </tr>
</table>
<!--    /таблица с аналитикой    -->