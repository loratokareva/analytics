
<script src="/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/rpie.js"></script>


<script type="text/javascript">

    var obj = {
        values: [15, 85],
        colors: ['#CAB786', '#C4C4C4'],
        animation: true,
        animationSpeed: 0,
        fillTextData: true,
        fillTextColor: '#fff',
        fillTextAlign: 1.43,
        fillTextPosition: 'inner',
        doughnutHoleSize: 40,
        doughnutHoleColor: '#fff',
        offset: 0,
        pie: 'normal',
        isStrokePie: {
            stroke: 20,
            overlayStroke: false,
            overlayStrokeColor: '#fff',
            strokeStartEndPoints: 'No',
            strokeAnimation: false,
            strokeAnimationSpeed: 0,
            fontSize: '40px',
            textAlignement: 'center',
            fontFamily: 'Arial',
            fontWeight: 'bold'
        }
    };


    //Generate myCanvas
    generatePieGraph('myCanvas', obj);




</script>
</body>
</html>