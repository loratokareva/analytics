<?php
    $titlePage = 'Аналитика аренды';
    include('views/templates/header.inc.php');
    include('views/header.php');
?>

<main>

    <!--    заголовок раздела   -->
    <section class="container">
        <div class="row">
            <div class="col">
                <a href="/" class="logo">
                    <img src="img/logo.png" alt="">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="title-page">
                    <p>
                        Оптово-розничный продовольственный центр
                    </p>
                    <p>
                        Москва п. Сосенское, 22-й км. Калужского шоссе, здание №10
                    </p>
                </div>


            </div>
        </div>


    </section>
    <!--    /заголовок раздела   -->

    <!--    заголовок страницы  -->
    <section class="container indent-t">
        <div class="row">
            <div class="col">
                <div>
                    <h1>
                        Торговый центр
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!--    /заголовок страницы  -->


    <!--    блок аналитики   -->
    <article class="container indent">
        <div class="row">
            <div class="col">
                <?php
                    include('views/table-analytics.php');
                ?>
            </div>

            <div class="col">
                <!--    круг аналитики-->
                <div class="donut" style="background: conic-gradient(#CAB786 0deg 90deg, #C4C4C4 90deg 360deg);">
                    <div class="hole"></div>
                </div>
                <!--    /круг аналитики-->
            </div>
        </div>
    </article>
    <!--    /блок аналитики   -->


    <!--  табы  -->
    <section class="container indent">
        <div class="row">
            <div class="col">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <button class="nav-link active"
                                id="pills-1-tab"
                                data-bs-toggle="pill"
                                data-bs-target="#pills-1"
                                type="button"
                                aria-selected="true">Этаж 1
                        </button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link"
                                id="pills-2-tab"
                                data-bs-toggle="pill"
                                data-bs-target="#pills-2"
                                type="button"
                                aria-selected="false">Этаж 2
                        </button>
                    </li>

                    <li class="nav-item">
                        <button class="nav-link"
                                id="pills-3-tab"
                                data-bs-toggle="pill"
                                data-bs-target="#pills-3"
                                type="button"
                                aria-selected="false">без схемы
                        </button>
                    </li>

                    <li class="nav-item">
                        <button class="nav-link"
                                id="pills-4-tab"
                                data-bs-toggle="pill"
                                data-bs-target="#pills-4"
                                type="button"
                                aria-selected="false">цоколь
                        </button>
                    </li>

                    <li class="nav-item">
                        <button class="nav-link"
                                id="pills-5-tab"
                                data-bs-toggle="pill"
                                data-bs-target="#pills-5"
                                type="button"
                                aria-selected="false">КД 9
                        </button>
                    </li>

                    <li class="nav-item">
                        <button class="nav-link"
                                id="pills-6-tab"
                                data-bs-toggle="pill"
                                data-bs-target="#pills-6"
                                type="button"
                                aria-selected="false">КД 10
                        </button>
                    </li>

                    <li class="nav-item">
                        <button class="nav-link"
                                id="pills-contact-tab"
                                data-bs-toggle="pill"
                                data-bs-target="#pills-7"
                                type="button"
                                aria-selected="false">Мини опт
                        </button>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active"
                         id="pills-1">
                        Пример кнопок для схемы


                        <!--    пример кнопок   -->
                        <section class="container">
                            <div class="row g-0">
                                <div class="col">

                                    <div class="list-btn mt-3">
                                        <div class="list-btn__item">
                                            <button class="btn btn-free">
                                                Свободен
                                            </button>
                                        </div>
                                        <div class="list-btn__item">
                                            <button class="btn btn-booking">
                                                Бронь
                                            </button>
                                        </div>
                                        <div class="list-btn__item">
                                            <button class="btn btn-busy">
                                                Занят
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                        <!--    /пример кнопок   -->

                    </div>
                    <div class="tab-pane fade"
                         id="pills-2">
                        кнопка 2
                    </div>
                    <div class="tab-pane fade"
                         id="pills-3">
                        без схемы
                    </div>
                    <div class="tab-pane fade"
                         id="pills-4">
                        цоколь
                    </div>
                    <div class="tab-pane fade"
                         id="pills-5">
                        КД 9
                    </div>
                    <div class="tab-pane fade"
                         id="pills-6">
                        КД 10
                    </div>
                    <div class="tab-pane fade"
                         id="pills-7">
                        Мини опт
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  /табы  -->

</main>
<?php
    include('views/footer.php');
    include('views/templates/footer.inc.php');
?>