<?php
    $titlePage = 'Аналитика аренды';
    include('views/templates/header.inc.php');
    include('views/header.php');
?>

<main>

    <!--    заголовок раздела   -->
    <section class="container">
        <div class="row">
            <div class="col">
                <a href="/" class="logo">
                    <img src="img/logo.png" alt="">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="title-page">
                    <p>
                        Оптово-розничный продовольственный центр
                    </p>
                    <p>
                        Москва п. Сосенское, 22-й км. Калужского шоссе, здание №10
                    </p>
                </div>


            </div>
        </div>
    </section>
    <!--    /заголовок раздела   -->


    <!--    блок аналитики   -->
    <article class="container indent">
        <div class="row">
            <div class="col">
                <?php
                    include('views/table-analytics.php');
                ?>
            </div>

            <div class="col">
                <!--    круг аналитики-->
                <div class="donut" style="background: conic-gradient(#CAB786 0deg 90deg, #C4C4C4 90deg 360deg); display: none" >
                    <div class="hole"></div>
                </div>

                <div class="mypiechart">
                    <canvas id="myCanvas" width="180" height="180"></canvas>
                </div>

                <!--    /круг аналитики-->
            </div>
        </div>

    </article>
    <!--    /блок аналитики   -->

    <!--   image-map    -->
    <section class="container indent-b">
        <div class="row">
            <div class="col">
                <img src="/img/image-map.jpg" usemap="#image-map" class="img-fluid">
                <map name="image-map">
                    <area target="" alt="Складской терминал" title="Складской терминал" href="#warehouse-terminal"
                          coords="62,94,429,94,425,150,216,153,186,211,122,153,60,152" shape="poly">
                    <area target="" alt="Торговый центр" title="Торговый центр" href="#shopping-mall"
                          coords="598,66,1135,62,1133,119,822,122,809,175,687,125,598,124" shape="poly">
                    <area target="" alt="Очистные сооружения" title="Очистные сооружения" href="#treatment-facilities"
                          coords="34,427,170,430,169,396,224,426,272,428,268,532,34,533" shape="poly">
                    <area target="" alt="Оптовые склады" title="Оптовые склады" href="#wholesale-warehouses"
                          coords="36,556,264,553,421,481,363,555,434,553,428,608,34,612" shape="poly">
                    <area target="" alt="Гостиницы" title="Гостиницы" href="#hotels"
                          coords="523,552,561,549,504,463,621,548,755,549,755,602,522,604" shape="poly">
                    <area target="" alt="Складской комплекс" title="Складской комплекс" href="#warehouse-complex"
                          coords="793,550,880,550,710,454,1012,548,1307,548,1309,604,798,604" shape="poly">
                    <area target="" alt="Кросс-доки" title="Кросс-доки" href="#cross-docs"
                          coords="1004,462,1068,454,915,350,1163,459,1397,457,1398,505,1004,506" shape="poly">
                </map>
            </div>
        </div>
    </section>
    <!--    /image-map   -->

    <!--    блок фильтров   -->
    <section class="filters">
        <form action="">

            <div class="container indent-b">
                <div class="row justify-content-end">
                    <div class="col-3">
                        <section class="search">

                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Поиск объекта">
                                <button class="btn btn-search" type="button"></button>
                            </div>

                        </section>
                    </div>
                    <div class="col-3">
                        <select class="form-select">
                            <option selected>Объект</option>
                            <option value="1">Один</option>
                            <option value="2">Два</option>
                            <option value="3">Три</option>
                        </select>
                    </div>
                    <div class="col-3">

                        <select class="form-select">
                            <option value="1" selected>По алфавиту</option>
                            <option value="2">Два</option>
                            <option value="3">Три</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <!--    /блок фильтров   -->


    <section class="analytics-list">
        <!--    блок аналитики   -->
        <article class="container">
            <div class="row g-0">
                <div class="col">
                    <div id="shopping-mall" class="table-title mb-5">
                        <h3>
                            Торговый центр
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php
                        include('views/table-analytics.php');
                    ?>
                </div>

                <div class="col">
                    <!--    круг аналитики-->
                    <div class="donut" style="background: conic-gradient(#CAB786 0deg 90deg, #C4C4C4 90deg 360deg);">
                        <div class="hole"></div>
                    </div>
                    <!--    /круг аналитики-->
                </div>

                <div class="col-3 d-flex align-items-start justify-content-end">
                    <a href="page-inner.php" type="button" class="btn btn-outline-secondary">подробнее</a>
                </div>
                
            </div>

        </article>
        <!--    /блок аналитики   -->

        <!--    блок аналитики   -->
        <article class="container">
            <div class="row">
                <div class="col">
                    <div id="warehouse-terminal" class="table-title mb-5">
                        <h3>
                            Складской терминал
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php
                        include('views/table-analytics.php');
                    ?>
                </div>

                <div class="col">
                    <!--    круг аналитики-->
                    <div class="donut" style="background: conic-gradient(#CAB786 0deg 250deg, #C4C4C4 250deg 360deg);">
                        <div class="hole"></div>
                    </div>
                    <!--    /круг аналитики-->
                </div>

                <div class="col-3 d-flex align-items-start justify-content-end">
                    <a href="page-inner.php" type="button" class="btn btn-outline-secondary">подробнее</a>
                </div>


            </div>

        </article>
        <!--    /блок аналитики   -->

        <!--    блок аналитики   -->
        <article class="container">
            <div class="row">
                <div class="col">
                    <div id="wholesale-warehouses" class="table-title mb-5">
                        <h3>
                            Оптовые склады
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php
                        include('views/table-analytics.php');
                    ?>
                </div>

                <div class="col">
                    <!--    круг аналитики-->
                    <div class="donut" style="background: conic-gradient(#CAB786 0deg 250deg, #C4C4C4 250deg 360deg);">
                        <div class="hole"></div>
                    </div>
                    <!--    /круг аналитики-->
                </div>

                <div class="col-3 d-flex align-items-start justify-content-end">
                    <a href="page-inner.php" type="button" class="btn btn-outline-secondary">подробнее</a>
                </div>


            </div>

        </article>
        <!--    /блок аналитики   -->

        <!--    блок аналитики   -->
        <article class="container">
            <div class="row">
                <div class="col">
                    <div id="hotels" class="table-title mb-5">
                        <h3>
                            Гостиницы
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php
                        include('views/table-analytics.php');
                    ?>
                </div>

                <div class="col">
                    <!--    круг аналитики-->
                    <div class="donut" style="background: conic-gradient(#CAB786 0deg 250deg, #C4C4C4 250deg 360deg);">
                        <div class="hole"></div>
                    </div>
                    <!--    /круг аналитики-->
                </div>

                <div class="col-3 d-flex align-items-start justify-content-end">
                    <a href="page-inner.php" type="button" class="btn btn-outline-secondary">подробнее</a>
                </div>


            </div>

        </article>
        <!--    /блок аналитики   -->

        <!--    блок аналитики   -->
        <article class="container">
            <div class="row">
                <div class="col">
                    <div id="warehouse-complex" class="table-title mb-5">
                        <h3>
                            Складской комплекс
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php
                        include('views/table-analytics.php');
                    ?>
                </div>

                <div class="col">
                    <!--    круг аналитики-->
                    <div class="donut" style="background: conic-gradient(#CAB786 0deg 250deg, #C4C4C4 250deg 360deg);">
                        <div class="hole"></div>
                    </div>
                    <!--    /круг аналитики-->
                </div>

                <div class="col-3 d-flex align-items-start justify-content-end">
                    <a href="page-inner.php" type="button" class="btn btn-outline-secondary">подробнее</a>
                </div>


            </div>

        </article>
        <!--    /блок аналитики   -->

        <!--    блок аналитики   -->
        <article class="container">
            <div class="row">
                <div class="col">
                    <div id="cross-docs" class="table-title mb-5">
                        <h3>
                            Кросс-доки
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php
                        include('views/table-analytics.php');
                    ?>
                </div>

                <div class="col">
                    <!--    круг аналитики-->
                    <div class="donut" style="background: conic-gradient(#CAB786 0deg 250deg, #C4C4C4 250deg 360deg);">
                        <div class="hole"></div>
                    </div>
                    <!--    /круг аналитики-->
                </div>

                <div class="col-3 d-flex align-items-start justify-content-end">
                    <a href="page-inner.php" type="button" class="btn btn-outline-secondary">подробнее</a>
                </div>


            </div>

        </article>
        <!--    /блок аналитики   -->

        <!--    блок аналитики   -->
        <article class="container">
            <div class="row">
                <div class="col">
                    <div id="treatment-facilities" class="table-title mb-5">
                        <h3>
                            Очистные сооружения
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php
                        include('views/table-analytics.php');
                    ?>
                </div>

                <div class="col">
                    <!--    круг аналитики-->
                    <div class="donut" style="background: conic-gradient(#CAB786 0deg 250deg, #C4C4C4 250deg 360deg);">
                        <div class="hole"></div>
                    </div>
                    <!--    /круг аналитики-->
                </div>

                <div class="col-3 d-flex align-items-start justify-content-end">
                    <a href="page-inner.php" type="button" class="btn btn-outline-secondary">подробнее</a>
                </div>


            </div>

        </article>
        <!--    /блок аналитики   -->

    </section>

</main>
<?php
    include('views/footer.php');
    include('views/templates/footer.inc.php');
?>